textures/window1_2
{
	surfaceparm metalsteps
	cull none

	// A GRATE OR GRILL THAT CAN BE SEEN FROM BOTH SIDES
	{
		map textures/wizwin1_2.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA // same as blendFunc blend
		alphaFunc vertex
		rgbGen vertex
	}
}

textures/wizwin1_2
{
	surfaceparm metalsteps
	cull none

	// A GRATE OR GRILL THAT CAN BE SEEN FROM BOTH SIDES
	{
		map textures/wizwin1_2.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA // same as blendFunc blend
		alphaFunc vertex
		rgbGen vertex
	}
}

textures/wizwin1_8
{
	surfaceparm metalsteps
	cull none

	// A GRATE OR GRILL THAT CAN BE SEEN FROM BOTH SIDES
	{
		map textures/wizwin1_2.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA // same as blendFunc blend
		alphaFunc vertex
		rgbGen vertex
	}
}
