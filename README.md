# The DarkPlaces Mod

A Quake mod for the DarkPlaces engine

![Screenshot of monster deathmatch on dpdm1](docs/dm7-dpdm1.jpg)


## Getting Started

DarkPlaces mod requires the [DarkPlaces engine](https://hemebond.gitlab.io/darkplaces-www/) and Quake data.

```
darkplaces -game dpmod
```

To play the mod either start a singleplayer game and play through normal quake levels, or try the examples below. Typing `dphelp` in the game console will list all commands and settings.

Multiplayer connections work through NAT routers, to host a server behind NAT, please set your NAT router to forward port 26000 UDP to your machine, and everyone should be able to connect to your server then, also use sv_public 1 if you want it to show up on the server browser (LAN searching is not implemented yet).


### Commands

dpmod adds the following commands:

`+button3`
:   alt-fire on weapons

`+button4`
:   zoom

`+button5`
:   grapple in deathmatch

These can be bound to keys by typing, e.g., `bind z +button` into the console.


### CVARs

These CVARs control dpmod settings and all take effect immediately:

`bots [number]`
:   adds bots to the game, set back to 0 to remove them.

`spawnmonsters [number]`
:   creates the specified number of monsters in multiplayer game modes (useful for CTF with monsters thrown in the mix, or you can play deathmatch 7 instead). (note: won't take effect during a level if it was 0 when the level began, as the monsters aren't loaded)

`damagescale_playerdamage [number]`
:   multiplies damage done by players, so for example 0.5 would be half damage, and 4 would be quad damage.

`damagescale_monsterdamage [number]`
:   multiplies damage done by monsters, so for example 0.5 would be half damage, and 4 would be quad damage.

`damagescale_monsterhealth [number]`
:   multiplies health of monsters, so for example 0.5 would be half health, and 4 would be quad health. (fun for coop or just a harder deathmatch 7)


## Monster Deathmatch

![Screenshot of monster deathmatch on dpdm2](docs/dpmod-monsters.jpg)

Monster deathmatch adds randomly-spawning Quake monsters, including monster champions, to the deathmatch experience.

For example, after launching the game, type this into the console (`) to begin a monster deathmatch on the <i>dpdm2</i> map:

```
deathmatch 7
map dpdm2
```

Monsters will now spawn throughout the level and hunt you down.


## Deathmatch Bots

![Screenshot of deathmatch bots on dpdm1](docs/dpmod-bots.jpg)

Here we will start a new deathmatch game, with a maximum of 10 players, with three bot players:

```
maxplayers 10
deathmatch 1
map dpdm1
bots 3
```

The bots work in any simple quake map (they don't understand liquids, doors, lifts, etc) as long as there are enough item spawn points to navigate by, or if the map has waypoints.


## Nightmare Mode

![Screenshot of nightmare mode on e1m2](docs/dpmod-nightmare.jpg)

LadyHavoc's 'ghostly' speedmod for Quake Expo 2006.

The speedmodding theme was making a new scarier Quake experience, preferably inspired by H.P. Lovecraft's work, insanity is a favorite subject of his work.

```
skill 5
map e1m1
```

Unfortunately I didn't work in any new H.P. Lovecraft inspired stuff; although some might consider the new surprise at the end of each level to be vaguely H.P. Lovecraft inspired, and playing this mod may carry a risk of paranoia :)


## Credits

### Parts made by Tomaz (thanks Tomaz!)

* Shell casing model and other casings
* Weapon models updated (offset to side and muzzleflash removed)

### Parts "scavenged" from other mods
* Visible weapon player models from Quake Done Quicker (which took them from Deathmatch Essentials)
* Flag model from Threewave CTF (what's Capture The Flag without the authentic Threewave flags?)
* Plasma gun sounds from Nelno's Plasma Gun mod (although he ripped them from DOOM, of course)
* Burning sounds (sound/fire/launch.wav and sound/fire/burn.wav - ripped from Ultimate Quake, launch.wav is not currently used)
* Flag pickup and capture sounds from Threewave CTF
* Foot step sounds from Quake: Scourge of Armagon (mission pack 1, not used in it however... I doubt Ritual Entertainment minds me using them)
* Of course, if anyone of the above mentioned mod teams or companies wishs me to remove something, I will immediately remove it and try to find a replacement.

All other parts are either scavenged (and modified) from the classic quake files, or are completely my own work.
